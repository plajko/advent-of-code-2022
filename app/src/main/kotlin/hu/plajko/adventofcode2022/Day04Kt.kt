package hu.plajko.adventofcode2022

import com.google.common.collect.Range
import hu.plajko.utils.*

object Day04Kt {
    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day04.in".readLines()
            .parse("(\\d+)-(\\d+),(\\d+)-(\\d+)")
            .map { (a, b, c, d) ->
                Pair(Range.closed(a.toInt(), b.toInt()), Range.closed(c.toInt(), d.toInt()))
            }
        val part1 = input.count { (r1, r2) -> r1.encloses(r2) || r2.encloses(r1) }
        val part2 = input.count { (r1, r2) -> r1.isConnected(r2) }
        println(part1)
        println(part2)
//        AoC.submitSolution(4, 2, part2)
    }
}
