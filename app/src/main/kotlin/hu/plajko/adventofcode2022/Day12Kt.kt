package hu.plajko.adventofcode2022

import hu.plajko.utils.CharMatrix
import hu.plajko.utils.GraphWrapperKt
import hu.plajko.utils.readLines
import org.jgrapht.alg.shortestpath.DijkstraShortestPath

data class Point(val x:Int, val y:Int, val char: Char) {
    fun isStart(): Boolean { return 'S' == char }
    fun isEnd(): Boolean { return 'E' == char }
    fun height(): Char { return if (isStart()) 'a' else if (isEnd()) 'z' else char }
}

object Day12Kt {
    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day12.in".readLines()
        val matrix = CharMatrix.fromList(input)
        val lowestPoints = mutableListOf<Point>()
        var start: Point? = null
        var end: Point? = null
        val graph = GraphWrapperKt.newDirected<Point>()
        (0 until matrix.numRows).map(matrix::getRow).forEachIndexed { i, r ->
            (0 until matrix.numCols).map(matrix::getColumn).forEachIndexed { j, c ->
                val current = Point(i, j, r[j])
                val currentHeight = current.height()
                if (current.isStart()) { start = current }
                if (current.isEnd()) { end = current }
                if ('a' == currentHeight) { lowestPoints.add(current) }

                sequenceOf(-1 to 0, 1 to 0, 0 to -1, 0 to 1)
                    .map { (x, y) -> i + x to j + y }
                    .filter { (r, c) -> r in (0 until matrix.numRows) && c in (0 until matrix.numCols) }
                    .map { (r, c) -> Point(r, c, matrix.getRow(r)[c]) }
                    .filter { it.height() <= currentHeight + 1 }
                    .forEach { graph.addEdge(it, current) { 1.0 } }
            }
        }

        val allPaths = DijkstraShortestPath(graph.graph).getPaths(end)
        val part1 = allPaths.getPath(start).length
        println("part1: $part1")

        val part2 = lowestPoints.mapNotNull { allPaths.getPath(it)?.length }.min()
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(12, 2, part2)
    }
}
