package hu.plajko.adventofcode2022;

import hu.plajko.utils.Extensions;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day01 {

    public static void main(String[] args) {
        var in = "Day01.in".readLines().split(String::isEmpty).stream()
                .map(l -> l.stream().mapToInt(Integer::parseInt).sum())
                .sorted().toList();
        System.out.println(in.last());
        System.out.println(in.last(3).toIntStream().sum());
    }
}
