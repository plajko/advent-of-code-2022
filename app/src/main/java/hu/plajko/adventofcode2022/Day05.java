package hu.plajko.adventofcode2022;

import com.google.common.collect.Range;
import hu.plajko.utils.AoC;
import hu.plajko.utils.CharMatrix;
import hu.plajko.utils.Extensions;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day05 {

    private static String solve(Map<Integer, String> stack, List<List<Integer>> moves, Function<String, String> copyTransform) {
        var stackMap = new TreeMap<>(stack);
        moves.forEach(move -> {
            int index = move.get(0);
            var from = move.get(1);
            var to = move.get(2);
            var fromS = stackMap.get(from).split(l -> List.of(index));
            var toS = stackMap.get(to);
            stackMap.put(to, copyTransform.apply(fromS.get(0)) + toS);
            stackMap.put(from, fromS.get(1));
        });
        return stackMap.values().stream().map(s -> s.substring(0, 1)).collect(Collectors.joining());
    }

    public static void main(String[] args) {
        var input = "Day05.in".readLines().split(String::isEmpty);
        var stacksInput = CharMatrix.fromList(input.get(0));
        var stacks = new ArrayList<String>();
        for (int i = 1; i < stacksInput.getNumCols(); i += 4) {
            stacks.add(stacksInput.getColumn(i).trim());
        }
        var stackMap = stacks.stream().collect(
            Collectors.groupingBy(s -> s.substring(s.length() - 1).asInt(),
            Collectors.reducing("", (a, b) -> b))
        );

        var moves = input.get(1).stream()
            .map("move (\\d+) from (\\d+) to (\\d+)".asMatcher())
            .map(m -> List.of(m.g(1).asInt(), m.g(2).asInt(), m.g(3).asInt()))
            .toList();

        var solution1 = solve(stackMap, moves, s -> new StringBuilder(s).reverse().toString());
        System.out.println(solution1);
        var solution2 = solve(stackMap, moves, s -> s);
        System.out.println(solution2);
//        AoC.submitSolution(5, 2, solution2);
    }
}
