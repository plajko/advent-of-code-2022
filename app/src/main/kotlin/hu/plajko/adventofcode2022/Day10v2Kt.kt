package hu.plajko.adventofcode2022

import hu.plajko.utils.readLines

object Day10v2Kt {

    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day10.in".readLines().map { ("$it 0").split(" ") }
            .flatMap { (cmd, num) -> if (cmd.startsWith("noop")) listOf(0) else listOf(0, num.toInt()) }

        val part1v1 = (20..220 step 40).sumOf { cycle -> (1 + input.take(cycle - 1).sum()) * cycle }

        val part1v2 = input.asSequence().runningFold(1) { x, num -> x + num }
            .foldIndexed(0) { i, sum, x -> (i + 1).let { cycle ->
                    sum + if (cycle in (20..220 step 40)) cycle * x else 0
                }
            }

        val (_, part1v3) = input.foldIndexed(1 to 0) { i, (x, sum), num -> (i + 1).let { cycle ->
                 (x + num) to (sum + if (cycle in (20..220 step 40)) cycle * x else 0)
            }
        }

        val part2 = input.foldIndexed(0 to "") { cycle, (x, pixels), num ->
            (x + num) to (pixels + if (cycle % 40 in (x until x + 3)) "#" else " ")
        }.second.chunked(40).joinToString("\n")

        println("part1: $part1v1")
        println("part1: $part1v2")
        println("part1: $part1v3")
        println("part2: \n$part2")
    }
}
