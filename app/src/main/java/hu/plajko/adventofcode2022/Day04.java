package hu.plajko.adventofcode2022;

import com.google.common.collect.Range;
import hu.plajko.utils.AoC;
import hu.plajko.utils.Extensions;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day04 {

    public static void main(String[] args) {
        var input = "Day04.in".readLines().stream()
                .map("(\\d+)-(\\d+),(\\d+)-(\\d+)".asMatcher())
                .map(m -> List.of(
                    Range.closed(m.g(1).asInt(), m.g(2).asInt()),
                    Range.closed(m.g(3).asInt(), m.g(4).asInt())))
                .toList();
        long part1 = 0;
        long part2 = 0;
        for (var ranges : input) {
            var r1 = ranges.get(0);
            var r2 = ranges.get(1);
            if (r1.encloses(r2) || r2.encloses(r1)) {
                part1++;
            }
            if (r1.isConnected(r2)) {
                part2++;
            }
        }
        System.out.println(part1);
        System.out.println(part2);
//        AoC.submitSolution(4, 2, part2);
    }
}
