package hu.plajko.adventofcode2022

import hu.plajko.utils.CharMatrix
import hu.plajko.utils.parse
import hu.plajko.utils.readLines
import kotlin.math.max
import kotlin.math.min
import kotlin.streams.asSequence

object Day14Kt {

    data class Input(val m: CharMatrix, val minX: Int, val maxX: Int, val minY: Int, val maxY:Int)

    fun parse(xMinPlus: Int, xMaxPlus: Int): Input {
        val input = "Day14.in".readLines().map { line ->
            line.split(" -> ").parse("(\\d+),(\\d+)").map { (a, b) ->
                a.toInt() to b.toInt()
            }
        }
        val minX: Int = input.flatten().minBy { it.first }.first - xMinPlus
        val maxX: Int = input.flatten().maxBy { it.first }.first + xMaxPlus
        val maxY: Int = input.flatten().maxBy { it.second }.second + 2
        val minY = 0
        val matrix = CharMatrix(maxY - minY + 1, maxX - minX + 1)
        input.forEach { rocks ->
            rocks.windowed(2).forEach { (a, b) ->
                if (a.second == b.second) {
                    val row = matrix.getRow(a.second - minY)
                    val i1 = min(a.first, b.first) - minX
                    val i2 = max(a.first, b.first) - minX
                    matrix.setRow(a.second - minY, row.replaceRange(i1, i2 + 1, "#".repeat(i2 - i1 + 1)))
                } else if (a.first == b.first) {
                    val col = matrix.getColumn(a.first - minX)
                    val i1 = min(a.second, b.second) - minY
                    val i2 = max(a.second, b.second) - minY
                    matrix.setColumn(a.first - minX, col.replaceRange(i1, i2 + 1, "#".repeat(i2 - i1 + 1)))
                } else throw IllegalStateException()
            }
        }
        return Input(matrix, minX, maxX, minY, maxY)
    }

    @JvmStatic
    fun main(args: Array<String>) {
        val part1input = parse(200,200)
        while(step(part1input.m, 0,500 - part1input.minX, generateSequence { 'O'.code }.iterator())) { }
        val part1 = part1input.m.stream().asSequence().count { it.value == 'O'.code }
        println("part1: $part1")

        val part2input = parse(200, 200)
        // add floor
        part2input.m.setRow(part2input.maxY, "#".repeat(part2input.m.numCols))
        while(step(part2input.m, 0,500 - part2input.minX, generateSequence { 'O'.code }.iterator())) { }
        val part2 = part2input.m.stream().asSequence().count { it.value == 'O'.code }
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(14, 1, part1)

        // render animation frames
        val animationMinX = part2input.m.stream().asSequence().filter { it.value == 'O'.code }.minBy { it.column }
        val animationMaxX = part2input.m.stream().asSequence().filter { it.value == 'O'.code }.maxBy { it.column }
        val margin = 3
        val animationInput = parse(
            200 - animationMinX.column + 1 + margin,
            200 - (part2input.m.numCols - animationMaxX.column) + 2 + margin)
        animationInput.m.setRow(animationInput.maxY, "#".repeat(animationInput.m.numCols))
        Day14AnimationKt.generateFrames(animationInput.m,500 - animationInput.minX, part2)
    }

    fun step(state: CharMatrix, row: Int, column: Int, pixelIterator: Iterator<Int>): Boolean {
        val air = setOf('.', '@')
        val floor = (row until state.numRows).firstOrNull { state.get(it, column) !in air } ?: -1
        (row until floor).forEach { state.set(it, column, '@') }
        return when {
            floor < 0 -> false // no floor found
            state.get(floor,column - 1) in air -> step(state, floor, column - 1, pixelIterator)
            state.get(floor,column + 1) in air -> step(state, floor, column + 1, pixelIterator)
            else -> (floor > 1).also{ state.set(floor - 1, column, pixelIterator.next()) }
        }
    }

}
