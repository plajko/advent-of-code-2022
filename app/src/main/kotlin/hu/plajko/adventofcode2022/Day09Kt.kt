package hu.plajko.adventofcode2022

import hu.plajko.utils.parse
import hu.plajko.utils.readLines
import kotlin.math.absoluteValue
import kotlin.math.sign

data class Knot(val x: Int = 0, val y: Int = 0)
typealias Direction = Knot
operator fun Knot.plus(p: Knot) = Knot(x + p.x, y + p.y)
operator fun Knot.minus(p: Knot) = Knot(x - p.x, y - p.y)
fun Knot.sign() = Knot(x.sign, y.sign)

object Day09Kt {

    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day09.in".readLines().parse("(.) (\\d+)").map { (directionString, amount) ->
            val direction = when(directionString) {
                "L" -> Direction(-1, 0)
                "R" -> Direction(1, 0)
                "U" -> Direction(0, 1)
                "D" -> Direction(0, -1)
                else -> throw IllegalStateException()
            }
            Pair(amount.toInt(), direction)
        }

        fun solve(ropeLength: Int) {
            val rope = (1..ropeLength).map{ Knot() }.toMutableList()
            input.flatMap { (amount, direction) ->
                (1..amount).map {
                    rope[0] = rope[0] + direction
                    (1 until rope.size).forEach { i ->
                        val diff = rope[i - 1] - rope[i]
                        if (diff.x.absoluteValue > 1 || diff.y.absoluteValue > 1) {
                            rope[i] = rope[i] + diff.sign()
                        }
                    }
                    rope.last()
                }.toSet()
            }.toSet().size
        }

        val part1 = solve(2)
        println("part1: $part1")

        val part2 = solve(10)
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(9, 2, part2)
    }
}
