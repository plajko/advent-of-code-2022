package hu.plajko.adventofcode2022

import hu.plajko.utils.CharMatrix
import hu.plajko.utils.readLines
import java.io.File

object Day10Kt {

    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day10.in".readLines().map { ("$it 0").split(" ") }
            .map { (cmd, num) -> Pair(cmd, num.toInt()) }

        var part1 = 0
        val part1Cycles = (20..220 step 40)
        var x = 1
        var cycle = 0

        val crt = (40..240 step 40)
        val screen = mutableListOf<String>()

        var line = ""

        fun cycle() {
            cycle++
            // part1
            if (cycle in part1Cycles) {
                part1 += cycle * x
            }
            // part2
            val pixelPos = (cycle -1) % 40
            val xPos = (x - 1) % 40
            line += if (xPos <= pixelPos && pixelPos < xPos + 3) "#" else " "
            if (cycle in crt) {
                screen.add(line)
                line = ""
            }
        }

        for ((cmd, num) in input) {
            when (cmd) {
                "noop" -> cycle()
                "addx" -> repeat(2){ cycle() }.also { x += num }
                else -> throw IllegalStateException()
            }
        }

        val part2 = screen.joinToString("\n")
        CharMatrix.fromList(screen).renderImage(File("Day10.png"), mapOf('#'.code to "#000000")::get);

        println("part1: $part1")
        println("part2: \n$part2")
//        hu.plajko.utils.AoCKt.submitSolution(10, 2, "EGJBGCFK")
    }
}
