package hu.plajko.adventofcode2022

import hu.plajko.utils.CharMatrix
import hu.plajko.utils.parse
import java.io.File
import kotlin.math.pow
import kotlin.streams.asSequence

object Day14AnimationKt {

    data class Color(val r: Double, val g: Double, val b: Double) {
        fun hex(): String {
            return "#" +
                    (256 * r).toInt().toString(16).padStart(2, '0') +
                    (256 * g).toInt().toString(16).padStart(2, '0') +
                    (256 * b).toInt().toString(16).padStart(2, '0')
        }
    }

    fun parseColor(colorString: String): Color {
        val (r, g, b) = colorString.parse("#(.{2})(.{2})(.{2})")
        return Color(
            r.toInt(16).toDouble() / 256.0,
            g.toInt(16).toDouble() / 256.0,
            b.toInt(16).toDouble() / 256.0
        )
    }
    fun getColor(cs1: String, cs2: String, percent: Double): String {
        val c1 = parseColor(cs1)
        val c2 = parseColor(cs2)
        val r = c1.r + percent * (c2.r - c1.r)
        val g = c1.g + percent * (c2.g - c1.g)
        val b = c1.b + percent * (c2.b - c1.b)
        return Color(r, g, b).hex()
    }

    fun generateFrames(matrix: CharMatrix, column: Int, frameCount: Int) {
        val start = 6
        val base = 300.0.pow(1.0 / (frameCount / start))
        val pixelIterator = generateSequence(1000) { it + 1 }.iterator()
        val frameIndexes = generateSequence(0) { it + 1 }.map { start * base.pow(it).toInt() }.iterator()
        var frame = 0
        var index = 0;
        var frameIndex = frameIndexes.next()
        val colorTemplate: (Int) -> String? = {
            when {
                it >= 1000 -> getColor("#C2B280", "#91835a", (index - (it - 1000)).toDouble() / frameCount.toDouble())
                it == 'O'.code -> "#C2B280"
                it == '@'.code -> "#9c8e65"
                it == '#'.code -> "#2f2f2f"
                else -> "#e2e2e2"
            }
        }
        fun render() { matrix.renderImage(File("animation/frame${frame++}.png"), colorTemplate) }
        while(Day14Kt.step(matrix, 0, column, pixelIterator)) {
            if(index == frameIndex) {
                render()
                frameIndex += frameIndexes.next()
            }
            matrix.stream().forEach { it.setIf('.') { c -> c.value == '@'.code } }
            index++;
        }
        val animation = matrix.stream().asSequence().count { it.value >= 1000 }
        println("animation: $animation")
        render()
    }
}
