package hu.plajko.adventofcode2022

import hu.plajko.utils.CharMatrix
import hu.plajko.utils.readLines
import hu.plajko.utils.split
import java.util.Collections

object Day22Kt {

    const val sample = false

    val tileSize = if (sample) 4 else 50

    private fun <T> List<T>.rotate(distance: Int) = this.toMutableList().also { Collections.rotate(it, distance) }.toList()

    @JvmStatic
    fun main(args: Array<String>) {
        val (mapLines, instLines) = "Day22${if (sample) "s" else ""}.in".readLines().split(String::isEmpty)
        val map = CharMatrix.fromList(mapLines)
        val instructions = instLines.first().split("\\d+".toRegex())
            .zip(instLines.first().split("[RL]".toRegex()))

        data class Pos(val r: Int, val c: Int)

        fun Pos.right() = Pos(c, tileSize - 1 - r)
        fun Pos.left() = Pos(tileSize - 1 - c, r)
        data class Tile(val id: Int, val start: Pos, val m: CharMatrix) {
            fun global(local: Pos): Pos = Pos(local.r + start.r, local.c + start.c)
            fun charAtLocal(local: Pos): Char = m.get(local.r, local.c)
        }
        data class EdgeMapping(
            val xNext: Map<Int, Pair<Int, (Pos, List<String>) -> Pair<Pos, List<String>>>>,
            val xPrev: Map<Int, Pair<Int, (Pos, List<String>) -> Pair<Pos, List<String>>>>,
            val yNext: Map<Int, Pair<Int, (Pos, List<String>) -> Pair<Pos, List<String>>>>,
            val yPrev: Map<Int, Pair<Int, (Pos, List<String>) -> Pair<Pos, List<String>>>>
       )

        fun getSubMatrix(r: Int, c: Int): Pair<Pos, CharMatrix> = Pos(r * tileSize, c * tileSize) to
                map.subMatrix(r * tileSize, c * tileSize, (r + 1) * tileSize, (c + 1) * tileSize)

        val tiles = if (!sample) listOf(
            getSubMatrix(0, 1).let { (start, m) -> Tile(0, start, m) },
            getSubMatrix(0, 2).let { (start, m) -> Tile(1, start, m) },
            getSubMatrix(1, 1).let { (start, m) -> Tile(2, start, m) },
            getSubMatrix(2, 0).let { (start, m) -> Tile(3, start, m) },
            getSubMatrix(2, 1).let { (start, m) -> Tile(4, start, m) },
            getSubMatrix(3, 0).let { (start, m) -> Tile(5, start, m) }
        ) else listOf(
            getSubMatrix(0, 2).let { (start, m) -> Tile(0, start, m) },
            getSubMatrix(1, 0).let { (start, m) -> Tile(1, start, m) },
            getSubMatrix(1, 1).let { (start, m) -> Tile(2, start, m) },
            getSubMatrix(1, 2).let { (start, m) -> Tile(3, start, m) },
            getSubMatrix(2, 2).let { (start, m) -> Tile(4, start, m) },
            getSubMatrix(2, 3).let { (start, m) -> Tile(5, start, m) }
        )

        fun noop(pos: Pos, dirs: List<String>): Pair<Pos, List<String>> = pos to dirs
        fun left(pos: Pos, dirs: List<String>): Pair<Pos, List<String>> = pos.left() to dirs.rotate(1)
        fun right(pos: Pos, dirs: List<String>): Pair<Pos, List<String>> = pos.right() to dirs.rotate(-1)
        fun half(pos: Pos, dirs: List<String>): Pair<Pos, List<String>> = pos.right().right() to dirs.rotate(-2)

        val edgeMapping2D = EdgeMapping(
            mapOf(0 to (1 to ::noop), 1 to (0 to ::noop), 2 to (2 to ::noop),
                3 to (4 to ::noop), 4 to (3 to ::noop), 5 to (5 to ::noop)),
            mapOf(0 to (1 to ::noop), 1 to (0 to ::noop), 2 to (2 to ::noop),
                3 to (4 to ::noop), 4 to (3 to ::noop), 5 to (5 to ::noop)),
            mapOf(0 to (2 to ::noop), 1 to (1 to ::noop), 2 to (4 to ::noop),
                3 to (5 to ::noop), 4 to (0 to ::noop), 5 to (3 to ::noop)),
            mapOf(0 to (4 to ::noop), 1 to (1 to ::noop), 2 to (0 to ::noop),
                3 to (5 to ::noop), 4 to (2 to ::noop), 5 to (3 to ::noop))
        )
        val edgeMapping3D = EdgeMapping(
            mapOf(0 to (1 to ::noop), 1 to (4 to ::half), 2 to (1 to ::left),
                3 to (4 to ::noop), 4 to (1 to ::half), 5 to (4 to ::left)),
            mapOf(0 to (3 to ::half), 1 to (0 to ::noop), 2 to (3 to ::left),
                3 to (0 to ::half), 4 to (3 to ::noop), 5 to (0 to ::left)),
            mapOf(0 to (2 to ::noop), 1 to (2 to ::right), 2 to (4 to ::noop),
                3 to (5 to ::noop), 4 to (5 to ::right), 5 to (1 to ::noop)),
            mapOf(0 to (5 to ::right), 1 to (5 to ::noop), 2 to (0 to ::noop),
                3 to (2 to ::right), 4 to (2 to ::noop), 5 to (3 to ::noop))
        )
        val edgeMappingSample2D = EdgeMapping(
            mapOf(0 to (0 to ::noop), 1 to (2 to ::noop), 2 to (3 to ::noop),
                3 to (1 to ::noop), 4 to (5 to ::noop), 5 to (4 to ::noop)),
            mapOf(0 to (0 to ::noop), 1 to (3 to ::noop), 2 to (1 to ::noop),
                3 to (2 to ::noop), 4 to (5 to ::noop), 5 to (4 to ::noop)),
            mapOf(0 to (3 to ::noop), 1 to (1 to ::noop), 2 to (2 to ::noop),
                3 to (4 to ::noop), 4 to (0 to ::noop), 5 to (5 to ::noop)),
            mapOf(0 to (4 to ::noop), 1 to (1 to ::noop), 2 to (2 to ::noop),
                3 to (0 to ::noop), 4 to (3 to ::noop), 5 to (5 to ::noop))
        )
        val edgeMappingSample3D = EdgeMapping(
            mapOf(0 to (5 to ::half), 1 to (2 to ::noop), 2 to (3 to ::noop),
                3 to (5 to ::right), 4 to (5 to ::noop), 5 to (0 to ::half)),
            mapOf(0 to (2 to ::left), 1 to (5 to ::right), 2 to (1 to ::noop),
                3 to (2 to ::noop), 4 to (2 to ::right), 5 to (4 to ::noop)),
            mapOf(0 to (3 to ::noop), 1 to (4 to ::half), 2 to (4 to ::left),
                3 to (4 to ::noop), 4 to (1 to ::half), 5 to (1 to ::left)),
            mapOf(0 to (1 to ::half), 1 to (0 to ::half), 2 to (0 to ::right),
                3 to (0 to ::noop), 4 to (3 to ::noop), 5 to (3 to ::left))
        )
        val directions = listOf("R", "D", "L", "U")
        fun runSteps(edgeMapping: EdgeMapping): Pair<Pos, String> {
            var tile = tiles[0]
            var localPos = Pos(0, 0)
            var dirs = directions

            instructions.forEach { (dirChange, amount) ->
                when (dirChange) {
                    "R" -> dirs = dirs.rotate(-1)
                    "L" -> dirs = dirs.rotate(1)
                }
                val steps = amount.toInt()

                var nextTile = tile
                var nextLocalPos = localPos
                var nextDirs = dirs
                var stepCounter = 0

                while (nextTile.charAtLocal(nextLocalPos) != '#' && stepCounter++ <= steps) {
                    tile = nextTile
                    localPos = nextLocalPos
                    dirs = nextDirs

                    fun nextState(target: Tile, pos: Pos, rotate: (Pos, List<String>) -> Pair<Pos, List<String>>) {
                        val result = rotate(pos, dirs)
                        nextTile = target; nextLocalPos = result.first; nextDirs = result.second
                    }

                    when {
                        dirs[0] == "R" && localPos.c < tileSize - 1 -> { nextLocalPos = Pos(localPos.r, localPos.c + 1) }
                        dirs[0] == "R" && localPos.c == tileSize - 1 -> edgeMapping.xNext[tile.id]!!.also { (target, rotate ) -> nextState(tiles[target], Pos(localPos.r, 0), rotate)}
                        dirs[0] == "L" && localPos.c > 0 -> { nextLocalPos = Pos(localPos.r, localPos.c - 1) }
                        dirs[0] == "L" && localPos.c == 0 -> edgeMapping.xPrev[tile.id]!!.also { (target, rotate ) -> nextState(tiles[target], Pos(localPos.r, tileSize - 1), rotate)}
                        dirs[0] == "D" && localPos.r < tileSize - 1 -> { nextLocalPos = Pos(localPos.r + 1, localPos.c) }
                        dirs[0] == "D" && localPos.r == tileSize - 1 -> edgeMapping.yNext[tile.id]!!.also { (target, rotate ) -> nextState(tiles[target], Pos(0, localPos.c), rotate)}
                        dirs[0] == "U" && localPos.r > 0 -> { nextLocalPos = Pos(localPos.r - 1, localPos.c) }
                        dirs[0] == "U" && localPos.r == 0 -> edgeMapping.yPrev[tile.id]!!.also { (target, rotate ) -> nextState(tiles[target], Pos(tileSize - 1, localPos.c), rotate)}
                        else -> throw IllegalStateException()
                    }
                }
            }
            return tile.global(localPos) to dirs[0]
        }

        val part1Result = runSteps(if (sample) edgeMappingSample2D else edgeMapping2D)
        val part1 = 1000 * (part1Result.first.r  + 1) + 4 * (part1Result.first.c + 1) + directions.indexOf(part1Result.second)
        println("part1: $part1")

        val part2Result = runSteps(if (sample) edgeMappingSample3D else edgeMapping3D)
        val part2 = 1000 * (part2Result.first.r  + 1) + 4 * (part2Result.first.c + 1) + directions.indexOf(part2Result.second)
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(22, 2, part2)
    }
}
