package hu.plajko.utils

import org.jgrapht.Graph
import org.jgrapht.graph.DefaultDirectedWeightedGraph
import org.jgrapht.graph.DefaultUndirectedWeightedGraph
import org.jgrapht.graph.DefaultWeightedEdge
import java.util.function.Function
import java.util.function.UnaryOperator

class GraphWrapperKt<V, E>(constructor: (edgeType: Class<E>) -> Graph<V, E>, edgeType: Class<E>) {
    val graph: Graph<V, E>

    init {
        graph = constructor(edgeType)
    }

    fun addDoubleEdge(s: V, t: V, weight: UnaryOperator<Double>) {
        graph.addVertex(s)
        graph.addVertex(t)
        addEdge(s, t, weight)
        addEdge(t, s, weight)
    }

    fun addEdge(s: V, t: V, weight: UnaryOperator<Double>) {
        graph.addVertex(s)
        graph.addVertex(t)
        doAddEdge(s, t, weight)
    }

    private fun doAddEdge(s: V, t: V, weight: UnaryOperator<Double>) {
        val e = graph.getEdge(s, t)
        if (e == null) {
            graph.addEdge(s, t)
            graph.setEdgeWeight(s, t, weight.apply(0.0))
        } else {
            graph.setEdgeWeight(s, t, weight.apply(graph.getEdgeWeight(e)))
        }
    }

    companion object {
        fun <V> newDirected(): GraphWrapperKt<V, DefaultWeightedEdge> {
            return GraphWrapperKt({ DefaultDirectedWeightedGraph(it) }, DefaultWeightedEdge::class.java)
        }

        fun <V> newUndirected(): GraphWrapperKt<V, DefaultWeightedEdge> {
            return GraphWrapperKt({ DefaultUndirectedWeightedGraph(it) }, DefaultWeightedEdge::class.java)
        }
    }
}
