package hu.plajko.utils

import com.google.common.io.Resources
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path

fun String.readLines(): MutableList<String> {
    val path = try {
        Path.of(Resources.getResource(this).toURI())
    } catch (e: IllegalArgumentException) {
        val matcher = "Day(\\d+)(s?).in".toRegex().matchEntire(this)
        if (matcher != null && e.message?.contains("not found") == true) {
            val sample = matcher.groupValues[2] == "s"
            val day = matcher.groupValues[1].toInt()
            if (sample) AoCKt.downloadSampleInput(day) else AoCKt.downloadInput(day)
        } else throw e
    }
    return Files.readAllLines(path, StandardCharsets.UTF_8)
}
fun <T> List<T>.split(predicate: (T) -> Boolean): MutableList<List<T>> {
    return Extensions.split(this, predicate)
}
fun List<String>.parse(regexp: String): List<MatchResult.Destructured> {
    return this.parse(regexp.toRegex())
}
fun List<String>.parse(regexp: Regex): List<MatchResult.Destructured> {
    return this.map { s -> s.parse(regexp) }
}
fun String.parse(regexp: String): MatchResult.Destructured {
    return this.parse(regexp.toRegex())
}
fun String.parse(regexp: Regex): MatchResult.Destructured {
    return regexp.matchEntire(this)?.destructured ?: throw IllegalStateException("Parse error: $this")
}
