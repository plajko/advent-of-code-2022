package hu.plajko.adventofcode2022

import com.google.common.collect.Range
import hu.plajko.utils.parse
import hu.plajko.utils.readLines
import kotlin.math.absoluteValue

data class P(val x: Int, val y: Int, val z: Int) {
    fun isNeighbour(p: P): Boolean {
        val xa = (x - p.x).absoluteValue
        val ya = (y - p.y).absoluteValue
        val za = (z - p.z).absoluteValue
        return xa + ya + za == 1
    }
}

fun Collection<P>.rangeX() = this.minOf { it.x } .. this.maxOf { it.x }
fun Collection<P>.rangeY() = this.minOf { it.y } .. this.maxOf { it.y }
fun Collection<P>.rangeZ() = this.minOf { it.z } .. this.maxOf { it.z }
fun Collection<P>.rangeX(p:P) = Range.openClosed(
    this.filter { p.z == it.z && p.y == it.y }.minOfOrNull { it.x } ?: p.x,
    this.filter { p.z == it.z && p.y == it.y }.maxOfOrNull { it.x } ?: p.x)
fun Collection<P>.rangeY(p:P) = Range.openClosed(
    this.filter { p.x == it.x && p.z == it.z }.minOfOrNull { it.y } ?: p.y,
    this.filter { p.x == it.x && p.z == it.z }.maxOfOrNull { it.y } ?: p.y)
fun Collection<P>.rangeZ(p:P) = Range.openClosed(
    this.filter { p.x == it.x && p.y == it.y }.minOfOrNull { it.z } ?: p.z,
    this.filter { p.x == it.x && p.y == it.y }.maxOfOrNull { it.z } ?: p.z)

object Day18Kt {
    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day18.in".readLines().parse("(-?\\d+),(-?\\d+),(-?\\d+)")
            .map { (x, y, z) ->
                P(x.toInt(), y.toInt(), z.toInt())
            }

        fun countSurface(cubes: Collection<P>): Int {
            return cubes.size * 6 - cubes.map { c1 ->
                cubes.asSequence().filter { c2 ->
                    c1.isNeighbour(c2)
                }.take(6).count()
            }.sum()
        }

        var part1 = countSurface(input)
        println("part1: $part1")

        val inset = input.toMutableSet()
        val spaces = input.rangeX().flatMap { x ->
            input.rangeY().flatMap { y ->
                input.rangeZ().mapNotNull { z ->
                    val p = P(x, y, z)
                    if (!inset.contains(p)) {
                        val rangeX = inset.rangeX(p)
                        val rangeY = inset.rangeY(p)
                        val rangeZ = inset.rangeZ(p)
                        if (!inset.contains(p) && rangeX.contains(p.x) && rangeY.contains(p.y) && rangeZ.contains(p.z)) {
                            p
                        } else null
                    } else null
                }
            }
        }.toSet()

        fun fillSpace(p: P, s: MutableSet<P> = mutableSetOf()): Set<P> {
            if (s.add(p)) {
                spaces.filter { p.isNeighbour(it) }.forEach {
                    fillSpace(it, s)
                }
            }
            return s
        }

        val drop = mutableSetOf<P>()
        spaces.forEach { p ->
            if (!drop.contains(p)) {
                inset.add(p)
                val count = inset.filter { c ->
                    p.isNeighbour(c)
                }.take(6).count()
                if (count == 6) {
                    drop.addAll(fillSpace(p))
                }
            }
        }

        val part2 = part1 - countSurface(drop)
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(18, 2, part1)
    }
}
