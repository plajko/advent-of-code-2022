package hu.plajko.utils;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.function.Function;
import java.util.function.UnaryOperator;

public class GraphWrapper<V, E> {

    private final Graph<V, E> graph;

    public  Graph<V, E> getGraph() {
        return this.graph;
    }

    public static <V> GraphWrapper<V, DefaultWeightedEdge> newDirected() {
        return new GraphWrapper<>(DefaultDirectedWeightedGraph::new, DefaultWeightedEdge.class);
    }

    public static <V> GraphWrapper<V, DefaultWeightedEdge> newUndirected() {
        return new GraphWrapper<>(DefaultUndirectedWeightedGraph::new, DefaultWeightedEdge.class);
    }

    public GraphWrapper(Function<Class<E>, Graph<V, E>> constructor, Class<E> edgeType) {
        this.graph = constructor.apply(edgeType);
    }

    public void addDoubleEdge(V s, V t, UnaryOperator<Double> weight) {
        this.graph.addVertex(s);
        this.graph.addVertex(t);
        addEdge(s, t, weight);
        addEdge(t, s, weight);
    }

    public void addEdge(V s, V t, UnaryOperator<Double> weight) {
        this.graph.addVertex(s);
        this.graph.addVertex(t);
        doAddEdge(s, t, weight);
    }

    private void doAddEdge(V s, V t, UnaryOperator<Double> weight) {
        var e = this.graph.getEdge(s, t);
        if (e == null) {
            this.graph.addEdge(s, t);
            this.graph.setEdgeWeight(s, t, weight.apply(0.0d));
        } else {
            this.graph.setEdgeWeight(s, t, weight.apply(this.graph.getEdgeWeight(e)));
        }
    }
}
