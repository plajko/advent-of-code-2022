package hu.plajko.adventofcode2022;

import com.google.common.collect.Lists;
import hu.plajko.utils.Extensions;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day03 {

    static int val(int c) {
        return c < 95 ? c - (int) 'A' + 27 : c - (int) 'a' + 1;
    }

    static int solve(List<List<String>> in) {
        int sum = 0;
        for (var l : in) {
            var common = l.get(0).chars().boxed().toSet();
            for (int i = 1; i < l.size(); i++) {
                common.retainAll(l.get(i).chars().boxed().toSet());
            }
            sum += common.stream().mapToInt(Day03::val).sum();
        }
        return sum;
    }

    public static void main(String[] args) {
        var in = "Day03.in".readLines();
        var part1 = solve(in.stream().map(s -> s.split(l -> List.of(l / 2))).toList());
        var part2 = solve(Lists.partition(in, 3));

        System.out.println(part1);
        System.out.println(part2);
//        AoC.submitSolution(3, 1, part1);
//        AoC.submitSolution(3, 2, part2);
    }
}
