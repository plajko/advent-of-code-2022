package hu.plajko.utils

import org.jsoup.Jsoup
import java.io.File
import java.net.URI
import java.net.URLEncoder
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.nio.file.Files
import java.nio.file.Path
import java.util.*

object AoCKt {

    private const val YEAR = 2022

    private fun getSessionId(): String {
        return "session_id".readLines().first()
    }

    fun downloadInput(day: Int): Path {
        val uri = URI("https://adventofcode.com/$YEAR/day/$day/input")
        println("Trying to fetch input from $uri")
        val request = HttpRequest.newBuilder()
            .header("Cookie", "session=${getSessionId()}")
            .uri(uri).build()
        val client = HttpClient.newBuilder().build()
        val resp = client.send(request, HttpResponse.BodyHandlers.ofByteArray())
        return if (resp.statusCode() == 200) {
            val fileName = "Day${day.toString().padStart(2, '0')}.in"
            File(File("app\\src\\main\\resources"), fileName).toPath().also {
                Files.write(it, resp.body())
            }
        } else throw IllegalArgumentException(String(resp.body(), Charsets.UTF_8))
    }

    fun downloadSampleInput(day: Int): Path {
        val uri = URI("https://adventofcode.com/$YEAR/day/$day")
        println("Trying to fetch sample input from $uri")
        val request = HttpRequest.newBuilder()
            .header("Cookie", "session=${getSessionId()}")
            .uri(uri).build()
        val client = HttpClient.newBuilder().build()
        val resp = client.send(request, HttpResponse.BodyHandlers.ofString(Charsets.UTF_8))
        return if (resp.statusCode() == 200) {
            val page = Jsoup.parse(resp.body())
            val sampleBlock = listOf("for example", "your puzzle input").flatMap {
                val marker = page.select("body > main > article > p").find { e -> e.text().lowercase().contains(it) }
                page.select("${marker?.cssSelector()} ~ pre > code")
            }.find { e -> e.hasText() }
            val sample = sampleBlock?.wholeText() ?: ""
            val fileName = "Day${day.toString().padStart(2, '0')}s.in"
            File(File("app\\src\\main\\resources"), fileName).toPath().also {
                Files.writeString(it, sample)
            }
        } else throw IllegalArgumentException(resp.body())
    }

    private fun getFormDataAsString(formData: Map<String, Any>): String {
        return formData.map { (key, value) ->
            "${URLEncoder.encode(key, Charsets.UTF_8)}=${URLEncoder.encode(value.toString(), Charsets.UTF_8)}"
        }.joinToString("&")
    }

    fun submitSolution(day: Int, level: Int, answer: String) {
        val uri = URI("https://adventofcode.com/$YEAR/day/$day/answer")
        val body = getFormDataAsString(mapOf("level" to level, "answer" to answer))
        val request = HttpRequest.newBuilder()
            .POST(HttpRequest.BodyPublishers.ofString(body, Charsets.UTF_8))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .header("Cookie", "session=${getSessionId()}")
            .uri(uri).build()
        val client = HttpClient.newBuilder().build()
        val resp = client.send(request, HttpResponse.BodyHandlers.ofString(Charsets.UTF_8))
        val page = Jsoup.parse(resp.body())
        println(page.select("body > main > article").text())
    }

    fun submitSolution(day: Int, level: Int, answer: Number) {
        submitSolution(day, level, answer.toString())
    }

}
