package hu.plajko.adventofcode2022

import com.google.common.collect.Range
import com.google.common.collect.Sets
import hu.plajko.utils.CharMatrix
import hu.plajko.utils.readLines
import hu.plajko.utils.split
import java.util.*
import kotlin.math.max
import kotlin.streams.asSequence
import kotlin.streams.toList

object Day17Kt {

    val shapeSequence = {
        generateSequence {
            "Day17_shapes.in".readLines().split(String::isEmpty).map { CharMatrix.fromList(it) }
        }.flatten().iterator()
    }
    val inputSequence = { generateSequence { "Day17.in".readLines().first().chars().toList() }.flatten().iterator() }

    data class State(var highest: Int, val rocks: MutableList<Shape>) {
        fun toMatrix(): CharMatrix {
            val miny = rocks.minOf { it.rangeY().lowerEndpoint() }
            val maxy = rocks.maxOf { it.rangeY().upperEndpoint() }
            val c = CharMatrix(maxy - miny + 1, 7)
            rocks.forEach { r ->
                r.points().forEach { p ->
                    val x = p.x
                    val y = c.numRows - 1 - p.y
                    c.set(y, x, '#')
                }
            }
            return c
        }
    }

    fun intersects(s1: Shape, s2: Shape): Boolean {
        val s1RangeX = s1.rangeX()
        val s1RangeY = s1.rangeY()
        val s2RangeX = s2.rangeX()
        val s2RangeY = s2.rangeY()
        if (!s1RangeX.isConnected(s2RangeX) || !s1RangeY.isConnected(s2RangeY)) {
            return false
        }
        val s1Points = s1.points()
        val s2Points = s2.points()
        return !Sets.intersection(s1Points, s2Points).isEmpty()
    }

    fun intersects(shape: Shape, state: State): Boolean {
        val rangeX = shape.rangeX()
        val rangeY = shape.rangeY()
        if (rangeY.contains(-1) || rangeX.contains(-1) || rangeX.contains(8)) {
            return true
        }
        return state.rocks.any { intersects(it, shape) }
    }

    data class Point(val x:Int, val y:Int)
    data class Shape(var pos: Point, val shape: CharMatrix) {
        fun rangeX(): Range<Int> = Range.closed(pos.x, pos.x + shape.numCols)
        fun rangeY(): Range<Int> = Range.closed( pos.y - shape.numRows + 1, pos.y)
        fun points(): Set<Point> = shape.stream().asSequence()
            .mapNotNull { if (it.value == '.'.code) null else Point(pos.x + it.column, pos.y - it.row) }
            .toSet()
        fun moveLeft(state: State) {
            val newPos = Point(pos.x - 1, pos.y)
            if (!intersects(Shape(newPos, this.shape), state)) {
                pos = newPos
            }
        }
        fun moveRight(state: State) {
            val newPos = Point(pos.x + 1, pos.y)
            if (!intersects(Shape(newPos, this.shape), state)) {
                pos = newPos
            }
        }
        fun moveDown(state: State): Boolean {
            val newPos = Point(pos.x, pos.y - 1)
            if (!intersects(Shape(newPos, this.shape), state)) {
                pos = newPos
                return true
            } else {
                return false
            }
        }
    }

    fun begin(shape: CharMatrix, state: State): Shape {
        return Shape(Point(2, state.highest + shape.numRows + 3 - 1), shape)
    }

    fun solve(rounds: Int): Pair<State, TreeMap<Int, Int>> {
        val shapes = shapeSequence()
        val input = inputSequence()
        val state = State(0, mutableListOf())
        val heightIndex = TreeMap<Int, Int>()
        var currentShape = begin(shapes.next(), state)
        var count = 0
        while (count < rounds) {
            when (input.next()) {
                '>'.code -> currentShape.moveRight(state)
                '<'.code -> currentShape.moveLeft(state)
                else -> throw IllegalStateException()
            }
            if (!currentShape.moveDown(state)) {
                state.highest = max(state.highest, currentShape.pos.y + 1)
                state.rocks.add(0, currentShape)
                currentShape = begin(shapes.next(), state)
                heightIndex[state.highest] = count
                count++
            }
        }
        return Pair(state, heightIndex)
    }


    @JvmStatic
    fun main(args: Array<String>) {
        val (statePart1) = solve(2022)
        val part1 = statePart1.highest
        println("part1: $part1")

        val (statePart2, heightIndex ) = solve(3000)
        val matrix = statePart2.toMatrix()
        val rowCodes = (matrix.numRows - 1 downTo 0).map { row ->
            matrix.getRow(row).chars()
                .mapToObj { if('#'.code == it) "1" else "0" }
                .toList().joinToString("").toInt(2)
        }

        // find repeating pattern
        val minimumLength = 30
        val (start, end) = (0 until rowCodes.size - minimumLength * 2).map { start ->
            start to ((start + minimumLength until rowCodes.size - minimumLength).find { end ->
                rowCodes.subList(start, start + minimumLength) == rowCodes.subList(end, end + minimumLength)
            } ?: -1)
        }.find{(_, end) -> end > 0} ?: throw IllegalStateException()
        val cycle = end - start

        // calculate height
        val startPartCount = heightIndex.floorEntry(start)
        val endPartCount = heightIndex.floorEntry(end)
        val cyclePartCount = endPartCount.value - startPartCount.value
        val limit = 1000000000000L - 1
        val cycleCount = (limit - startPartCount.value.toLong()) / cyclePartCount
        val remaining = (limit - startPartCount.value.toLong()) % cyclePartCount
        val remainingParts = heightIndex.tailMap(startPartCount.key).entries.find { it.value.toLong() - startPartCount.value == remaining }
        val part2 = start + cycle * cycleCount + (remainingParts!!.key - startPartCount.key)
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(17, 1, part1)
    }
}
