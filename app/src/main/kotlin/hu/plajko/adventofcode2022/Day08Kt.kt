package hu.plajko.adventofcode2022

import hu.plajko.utils.CharMatrix
import hu.plajko.utils.readLines
import kotlin.math.max

object Day08Kt {
    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day08.in".readLines()
        val matrix = CharMatrix.fromList(input)

        var part1 = 0
        var part2 = 0
        (0 until matrix.numRows).map(matrix::getRow).forEachIndexed { i, r ->
            (0 until matrix.numCols).map(matrix::getColumn).forEachIndexed { j, c ->
                val v = r[j].digitToInt()
                val left = r.take(j).reversed().map { it.digitToInt() }
                val right = r.drop(j + 1).map { it.digitToInt() }
                val up = c.take(i).reversed().map { it.digitToInt() }
                val down = c.drop(i + 1).map { it.digitToInt() }

                if (listOf(left, right, up, down).any { dir -> dir.all { it < v } }) part1++
                part2 = max(part2, listOf(left, right, up, down)
                    .map{ dir -> dir.dropLast(1).takeWhile { it < v }.size + 1 }
                    .reduce(Int::times))
            }
        }

        println("part1: $part1")
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(8, 2, part2)
    }
}
