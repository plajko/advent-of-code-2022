package hu.plajko.adventofcode2022

import hu.plajko.utils.readLines
import kotlin.streams.toList

object Day06Kt {
    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day06.in".readLines().first().chars().toList()
        val part1 = (4..input.size).find { input.subList(it - 4, it).toSet().size == 4 }
        println("part1: $part1")
        val part2 = (14..input.size).find { input.subList(it - 14, it).toSet().size == 14 }
        println("part2: $part2")
//        AoCKt.submitSolution(6, 2, 2803)
    }
}
