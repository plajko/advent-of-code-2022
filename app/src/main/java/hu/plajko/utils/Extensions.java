package hu.plajko.utils;

import com.google.common.base.Suppliers;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

@Slf4j
public class Extensions {

    @SneakyThrows
    public static List<String> readLines(String resourceFileName) {
        Path path;
        try {
            path = Path.of(Resources.getResource(resourceFileName).toURI());
        } catch (IllegalArgumentException e) {
            var matcher = allMatches("^Day(\\d+).in$", resourceFileName).findFirst();
            if (matcher.isPresent() && e.getMessage().contains("not found")) {
                path = AoC.downloadInput(Integer.parseInt(matcher.get().group(1)));
            } else {
                throw e;
            }
        }
        return Files.readAllLines(path, StandardCharsets.UTF_8);
    }

    public static <T> List<List<T>> split(List<T> list, Predicate<T> condition) {
        var result = new ArrayList<List<T>>();
        var part = new ArrayList<T>();
        for (var t : list) {
            if (condition.test(t)) {
                result.add(part);
                part = new ArrayList<>();
                continue;
            }
            part.add(t);
        }
        result.add(part);
        return result;
    }

    public static int asInt(String s) {
        return Integer.parseInt(s);
    }
    public static int asInt(String s, int radix) {
        return Integer.parseInt(s, radix);
    }
    public static long asLong(String s) {
        return Long.parseLong(s);
    }
    public static long asLong(String s, int radix) {
        return Long.parseLong(s, radix);
    }
    public static String pad(String s, int size) {
        return pad(s, size, ' ');
    }
    public static String pad(String s, int size, char c) {
        return IntStream.range(0, Math.max(0, size - s.length())).mapToObj(i -> c + "").collect(Collectors.joining()) + s;
    }
    public static BigInteger asBigInteger(String s) {
        return new BigInteger(s);
    }

    public static List<String> split(String s, Function<Integer, List<Integer>> indexSupplier) {
        var result = new ArrayList<String>();
        int start = 0;
        for (var end : indexSupplier.apply(s.length())) {
            result.add(s.substring(start, end));
            start = end;
        }
        result.add(s.substring(start));
        return result;
    }
    public static String replaceAll(String s, String regex, Function<MatchResult, ?> replacement) {
        return Pattern.compile(regex).matcher(s).replaceAll(replacement.andThen(String::valueOf));
    }
    public static Function<CharSequence, MatchResult> asMatcher(String regex) {
        return s -> allMatches(regex, s).findFirst().orElse(null);
    }
    public static Stream<MatchResult> allMatches(String regex, CharSequence s) {
        return Stream.generate(Suppliers.memoize(() -> Pattern.compile(regex).matcher(s)))
            .takeWhile(Matcher::find).map(Matcher::toMatchResult);
    }
    public static <T> Stream<T> allMatches(String regex, CharSequence s, Function<String, T> mapper) {
        return allMatches(regex, s).map(mapper.compose(MatchResult::group));
    }
    public static String g(MatchResult result, int group) {
        return result.group(group);
    }
    public static String g(MatchResult result) {
        return result.group();
    }
    public static <T> T first(List<T> list) {
        return list.isEmpty() ? null : list.get(0);
    }
    public static <T> T last(List<T> list) {
        return list.isEmpty() ? null : list.get(list.size() - 1);
    }
    public static <T> List<T> first(List<T> list, int n) {
        return list.isEmpty() ? null : list.subList(0, n);
    }
    public static <T> List<T> last(List<T> list, int n) {
        return list.isEmpty() ? null : list.subList(list.size() - n, list.size());
    }
    public static <T> List<T> toList(Stream<T> stream) {
        return stream.collect(Collectors.toList());
    }
    public static <T> Set<T> toSet(Stream<T> stream) {
        return stream.collect(Collectors.toSet());
    }
    public static <T> Stream<T> flatStream(List<List<T>> matrix) {
        return matrix.stream().flatMap(List::stream);
    }
    public static IntStream toIntStream(Collection<Integer> collection) {
        return toInt(collection.stream());
    }
    public static IntStream toInt(Stream<Integer> stream) {
        return stream.mapToInt(i -> i);
    }
    public static LongStream toLongStream(Collection<Long> collection) {
        return toLong(collection.stream());
    }
    public static LongStream toLong(Stream<Long> stream) {
        return stream.mapToLong(l -> l);
    }
    public static <T> List<List<T>> transpose(List<List<T>> matrix) {
        return IntStream.range(0, matrix.get(0).size())
            .mapToObj(i -> matrix.stream()
                .map(l -> l.get(i))
                .collect(Collectors.toList()))
            .collect(Collectors.toList());
    }
    public static <T> List<List<T>> rotateCW(List<List<T>> matrix) {
        var reverse = Lists.reverse(matrix);
        return IntStream.range(0, reverse.get(0).size())
            .mapToObj(i -> reverse.stream()
                .map(l -> l.get(i))
                .collect(Collectors.toList()))
            .collect(Collectors.toList());
    }
    public static <T> List<List<T>> rotateCCW(List<List<T>> matrix) {
        return IntStream.range(0, matrix.get(0).size())
            .mapToObj(i -> matrix.stream()
                .map(l -> l.get(l.size() - 1 - i))
                .collect(Collectors.toList()))
            .collect(Collectors.toList());
    }
    public static <T> void dump(List<List<T>> matrix) {
        var widths = IntStream.range(0, matrix.get(0).size())
            .mapToObj(i -> matrix.stream().mapToInt(r -> String.valueOf(r.get(i)).length()).max().getAsInt())
            .collect(Collectors.toList());
        System.out.println(matrix.stream()
            .map(r -> IntStream.range(0, r.size())
                .mapToObj(i -> pad(String.valueOf(r.get(i)), widths.get(i)))
                .collect(Collectors.joining(" ")))
            .collect(Collectors.joining("\n")) + "\n");
    }

    public static <T> void dump(BiFunction<Integer, Integer, T> provider, int x, int y) {
        var empty = " ";
        var widths = IntStream.range(0, y)
            .mapToObj(i -> IntStream.range(0, x)
                .map(j -> Optional.ofNullable(provider.apply(i, j))
                    .map(String::valueOf)
                    .orElse(empty).length())
                .max().orElseThrow())
            .collect(Collectors.toList());
        System.out.println(IntStream.range(0, x)
            .mapToObj(i -> IntStream.range(0, y)
                .mapToObj(j -> Optional.ofNullable(provider.apply(i, j))
                    .map(String::valueOf)
                    .orElse(empty))
                    .map(s -> pad(s, widths.get(i)))
                    .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n")) + "\n");
    }
}
