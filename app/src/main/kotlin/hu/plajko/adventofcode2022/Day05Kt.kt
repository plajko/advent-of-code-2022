package hu.plajko.adventofcode2022

import hu.plajko.utils.*
import java.util.TreeMap

object Day05Kt {
    @JvmStatic
    fun main(args: Array<String>) {
        val (stacksInput, movesInput) = "Day05.in".readLines().split(String::isEmpty)
        val stacksMatrix = CharMatrix.fromList(stacksInput)
        val stacks = (1 .. stacksMatrix.numCols step 4).map { stacksMatrix.getColumn(it).trim() }
            .associate { s -> s.takeLast(1).toInt() to s.dropLast(1) }
        val moves = movesInput
            .parse("move (\\d+) from (\\d+) to (\\d+)")
            .map { (index, from, to) -> Triple(index.toInt(), from.toInt(), to.toInt()) }

        fun applyMoves(transform: (String) -> String = { it }): TreeMap<Int, String> {
            return moves.fold(TreeMap(stacks)) { stackMap, (itemIndex, fromIndex, toIndex) ->
                stackMap.apply {
                    computeIfPresent(fromIndex) { _, from ->
                        computeIfPresent(toIndex) { _, to -> transform(from.take(itemIndex)) + to }
                        from.drop(itemIndex)
                    }
                }
            }
        }

        val part1 = applyMoves(String::reversed).values.map(String::first).joinToString("")
        println("part1: $part1")
        val part2 = applyMoves().values.map(String::first).joinToString("")
        println("part2: $part2")
//        AoCKt.submitSolution(5,2, part2)
    }
}
