package hu.plajko.adventofcode2022

import hu.plajko.utils.parse
import hu.plajko.utils.readLines

object Day21Kt {

    data class Cmd(val monkey: String, var v: Long?, var op1: String?, var op2: String?, var op: String?)

    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day21.in".readLines().parse("(.*): (.*)").associate { (monkey, cmd) ->
            if(cmd.matches("\\d+".toRegex())) {
                monkey to Cmd(monkey, cmd.toLong(), null ,null ,null)
            }else {
                val (op1, op, op2) = cmd.parse("(.*) (.) (.*)")
                monkey to Cmd(monkey,null, op1, op2, op)
            }
        }

        fun calc(c: Cmd): Long {
            return when{
                c.v != null -> c.v!!
                else -> {
                    val op1 = calc(input[c.op1]!!)
                    val op2 = calc(input[c.op2]!!)
                    when (c.op) {
                        "+" -> op1 + op2
                        "-" -> op1 - op2
                        "*" -> op1 * op2
                        "/" -> op1 / op2
                        else -> throw IllegalStateException()
                    }
                }
            }
        }

        fun hasHumn(c: Cmd): Boolean = when {
            c.monkey == "humn" -> true
            c.v != null -> false
            else -> hasHumn(input[c.op1]!!) || hasHumn(input[c.op2]!!)
        }

        fun solve(c: Cmd, n: Long = 0): Long {
            return when {
                c.monkey == "humn" -> n
                c.v != null -> c.v!!
                else -> {
                    val hasHumn1 = hasHumn(input[c.op1]!!)
                    val hasHumn2 = hasHumn(input[c.op2]!!)
                    when {
                        !hasHumn1 && !hasHumn2 -> calc(c)
                        hasHumn1 && !hasHumn2 -> {
                            val op2 = calc(input[c.op2]!!)
                            val nn = when {
                                c.monkey == "root" -> op2
                                c.op == "+" -> n - op2
                                c.op == "-" -> n + op2
                                c.op == "*" -> n / op2
                                c.op == "/" -> n * op2
                                else -> throw IllegalStateException()
                            }
                            solve(input[c.op1]!!, nn)
                        }
                        !hasHumn1 && hasHumn2 -> {
                            val op1 = calc(input[c.op1]!!)
                            val nn = when {
                                c.monkey == "root" -> op1
                                c.op == "+" -> n - op1
                                c.op == "-" -> op1 - n
                                c.op == "*" -> n / op1
                                c.op == "/" -> op1 / n
                                else -> throw IllegalStateException()
                            }
                            solve(input[c.op2]!!, nn)
                        }
                        else -> throw IllegalStateException()
                    }
                }
            }
        }

        val part1 = calc(input["root"]!!)
        println("part1: $part1")

        val part2 = solve(input["root"]!!)
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(21, 2, 434681062200212992/129789)
    }
}
