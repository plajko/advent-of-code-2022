package hu.plajko.adventofcode2022

import com.google.common.collect.ImmutableRangeSet
import com.google.common.collect.Range
import hu.plajko.utils.parse
import hu.plajko.utils.readLines
import kotlin.math.absoluteValue

data class Loc(val x: Int, val y: Int) {
    infix fun manhattan(other: Loc): Int = (x - other.x).absoluteValue + (y - other.y).absoluteValue
}
data class Sensor(val loc: Loc, val r: Int) {
    fun getXRange(y: Int): Range<Int>? {
        val dist = (loc.y - y).absoluteValue
        if (dist > r) { return null }
        return Range.closed(loc.x - (r - dist), loc.x + (r - dist))
    }
}

object Day15Kt {

    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day15.in".readLines()
            .parse("Sensor at x=(-?\\d+), y=(-?\\d+): closest beacon is at x=(-?\\d+), y=(-?\\d+)")
            .map { (sx, sy, bx, by) -> Pair(Loc(sx.toInt(), sy.toInt()), Loc(bx.toInt(), by.toInt())) }
            .map { (s, b) -> Sensor(s, s manhattan b) }

        val part1 = input.mapNotNull { s -> s.getXRange(2000000) }
            .let{ ImmutableRangeSet.unionOf(it) }
            .asRanges().sumOf { it.upperEndpoint() - it.lowerEndpoint() }
        println("part1: $part1")

        val part2Ranges = (0 .. 4000000).asSequence().map { y ->
            y to input.mapNotNull { s -> s.getXRange(y) }.let{ ImmutableRangeSet.unionOf(it) }
        }.find { (_, r) -> r.asRanges().size > 1 } ?: throw IllegalStateException()
        val part2 = part2Ranges.first + 4000000L * part2Ranges.second.asRanges().first().upperEndpoint()
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(15, 2, part2)
    }
}
