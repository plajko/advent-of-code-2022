package hu.plajko.adventofcode2022;

import hu.plajko.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day02 {

    @SneakyThrows
    public static void main(String[] args) {
        System.out.println(getScore(false));
        System.out.println(getScore(true));
    }

    static int getScore(boolean second) {
        var am = Map.of("A", "R", "B", "P", "C", "S");
        var bm = Map.of("X", "R", "Y", "P", "Z", "S");
        var sc = Map.of("R", 1, "P", 2, "S", 3);
        var in = "Day02.in".readLines().stream().map(s -> List.of("" + s.charAt(0), "" + s.charAt(2))).toList();
        int score = 0;
        for (var r : in) {
            var a = am.get(r.get(0));
            var b = r.get(1);

            if (second) {
                if ("X".equals(b)) {
                    b = "R".equals(a) ? "S" : "P".equals(a) ? "R" : "P";
                } else if ("Y".equals(b)) {
                    b = "R".equals(a) ? "R" : "P".equals(a) ? "P" : "S";
                } else if ("Z".equals(b)) {
                    b = "R".equals(a) ? "P" : "P".equals(a) ? "S" : "R";
                }
            } else {
                b = bm.get(b);
            }
            score += sc.get(b);
            if (a.equals(b)) {
                score += 3;
            } else if ("R".equals(a) && "P".equals(b)) {
                score += 6;
            } else if ("S".equals(a) && "R".equals(b)) {
                score += 6;
            } else if ("P".equals(a) && "S".equals(b)) {
                score += 6;
            }
        }
        return score;
    }
}
