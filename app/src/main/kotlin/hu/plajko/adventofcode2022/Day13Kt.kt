package hu.plajko.adventofcode2022

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import hu.plajko.utils.readLines
import hu.plajko.utils.split
import kotlin.math.min

object Day13Kt {

    private fun compare(a: JsonElement, b: JsonElement): Int {
        return when {
            a.isJsonPrimitive -> when {
                b.isJsonPrimitive -> if (a.asInt == b.asInt) 0 else (if (a.asInt > b.asInt) 1 else -1)
                b.isJsonArray -> compare(JsonArray().apply { add(a) }, b)
                else -> throw IllegalStateException()
            }
            a.isJsonArray -> when {
                b.isJsonPrimitive -> compare(a, JsonArray().apply { add(b) })
                b.isJsonArray -> {
                    val aArray = a.asJsonArray
                    val bArray = b.asJsonArray
                    (0 until min(aArray.size(), bArray.size())).forEach { it ->
                        val comp = compare(aArray[it], bArray[it])
                        if (comp != 0) {
                            return comp
                        }
                    }
                    return if (aArray.size() == bArray.size()) 0 else (if (aArray.size() > bArray.size()) 1 else -1)
                }
                else -> throw IllegalStateException()
            }
            else -> throw IllegalStateException()
        }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day13.in".readLines()

        val gson = Gson()
        fun parse(s: String): JsonElement { return gson.fromJson(s, JsonElement::class.java) }
        val part1 = input.split(String::isEmpty)
            .mapIndexed { i, (a, b) -> if (compare(parse(a), parse(b)) <= 0) i + 1 else 0 }.sum()
        println("part2: $part1")

        val allPackets = input.split(String::isEmpty).flatMap {(a, b) -> listOf(a, b) }.toMutableList()
        allPackets.add("[[2]]")
        allPackets.add("[[6]]")
        allPackets.sortWith { a, b -> compare(parse(a), parse(b)) }
        val part2 = (allPackets.indexOf("[[2]]") + 1) * (allPackets.indexOf("[[6]]") + 1)
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(13, 2, part2)
    }
}
