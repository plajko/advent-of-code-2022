package hu.plajko.adventofcode2022

import hu.plajko.utils.AoCKt
import hu.plajko.utils.parse
import hu.plajko.utils.readLines

object Day07Kt {
    @JvmStatic
    fun main(args: Array<String>) {
        val fileSys = mutableMapOf<String, Long>()
        val currentPath = mutableListOf<String>()
        "Day07.in".readLines().forEach { l ->
            if (l.startsWith("$ cd")) {
                val (dir) = l.parse("\\$ cd (.*)")
                if (".." == dir) currentPath.removeLast() else currentPath.add("$dir/")
            } else if (l.startsWith("dir ")) {
                val (dir) = l.parse("dir (.*)")
                fileSys[currentPath.joinToString("") + dir + "/"] = 0
            } else if (!l.startsWith("$")) {
                val (size, fileName) = l.parse("(\\d+) (.*)")
                fileSys[currentPath.joinToString("") + fileName] = size.toLong()
            }
        }
        val part1 = fileSys.keys.filter { it.endsWith("/") }.map { dir ->
            fileSys.filter { it.key.startsWith(dir) }.values.sum()
        }.filter { it < 100_000 }.sum()
        println("part1: $part1")

        val needed = 30_000_000 - (70_000_000 - fileSys.values.sum())
        val part2 = fileSys.keys.filter { it.endsWith("/") }.map { dir ->
            fileSys.filter { it.key.startsWith(dir) }.values.sum()
        }.filter { it >= needed }.min()
        println("part2: $part2")
//        AoCKt.submitSolution(7, 2, part2)
    }
}
