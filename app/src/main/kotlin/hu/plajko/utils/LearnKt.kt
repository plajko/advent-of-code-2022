package hu.plajko.utils

import kotlinx.coroutines.*
import org.jgrapht.alg.flow.DinicMFImpl
import org.jgrapht.alg.shortestpath.DijkstraShortestPath
import java.util.*
import java.util.concurrent.Executors

class LearnKt {
    companion object {

        data class V(val id: Int, val alma: String = "")

        @JvmStatic
        fun main(args: Array<String>) {
            val g = GraphWrapperKt.newDirected<V>()
            val r = Random()
            repeat(100000) {
                g.addDoubleEdge(V(r.nextInt(1000)), V(r.nextInt(1000))) { w -> if (w == 0.0) 1.0 else w / 2 }
            }
            val s = DijkstraShortestPath(g.graph)
            val p = s.getPath(V(r.nextInt(100)), V((r.nextInt(100))))
            println(p.weight.toString() + " - " + p.edgeList)
            val f = DinicMFImpl(g.graph)
            val mf = f.getMaximumFlow(V(r.nextInt(100)), V(r.nextInt(100)))
            println(mf.value.toString() + " - " + f.cutEdges.size)
            val (_, a) = V(1).copy(alma = "b")
            println(a)


            runBlocking {
                val asy: Deferred<String> = async {
                    println("c")
                    "aaaaaaaa"
                }
                launch {
                    println("b")
                }
                launch {
                    println("a")
                }
                println(asy.await())
            }

            suspend fun task1(): String {
                println("start task1 in Thread ${Thread.currentThread()}")
                yield()
//                delay(2000)
                println("end task1 in Thread ${Thread.currentThread()}")

                return "foobar"
            }
            suspend fun task2() {
                println("start task2 in Thread ${Thread.currentThread()}")
                yield()
                println("end task2 in Thread ${Thread.currentThread()}")
            }
            println("start")
            Executors.newSingleThreadExecutor().asCoroutineDispatcher().use { context ->
                runBlocking {
                    val results = mutableListOf<Deferred<String>>()

                    repeat(10) {
                        results.add(async(context) { task1() })
                    }

                    launch { task2() }

                    println("called task1 and task2 from ${Thread.currentThread()}")

                    // now wait for the results
                    println(results.joinToString(", ") { runBlocking { it.await() } })
                }
            }


            val l = mutableListOf<Int>()
            repeat(2000) {
                l.add(r.nextInt(100));
            }
            val index = l.groupingBy{(it%14)}.fold(setOf<Int>()){acc, i -> acc + i}
            println(index)
        }
    }
}
