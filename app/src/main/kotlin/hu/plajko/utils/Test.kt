package hu.plajko.utils


object Test {

    @JvmStatic
    fun main(args: Array<String>) {
        data class Data(val x: Int = 1, val y: Int = 2)
        println((1..3).map { Data() })
        println((1..3).map(::Data))
        println((1..3).map { Data(y = 1) })
        println((1..3).map { run(::Data) })
    }
}
