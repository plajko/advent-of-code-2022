package hu.plajko.adventofcode2022

import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import hu.plajko.utils.parse
import hu.plajko.utils.readLines
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.max

data class Blueprint(val id: Int, val costs: Map<String, Cost>) {
    private val cache = CacheBuilder.newBuilder().build<Robots, Cost>()
    fun costOf(robots: Robots): Cost {
        return cache.get(robots) {
            val ore = costs["ore"]!!
            val clay = costs["clay"]!!
            val obsidian = costs["obsidian"]!!
            val geode = costs["geode"]!!
            ore * robots.ore + clay * robots.clay + obsidian * robots.obsidian + geode * robots.geode
        }
    }
}

data class Amount(val ore: Int = 0, val clay: Int = 0, val obsidian: Int = 0, val geode: Int = 0) {
    operator fun plus(v: Amount): Amount =
        Amount(ore + v.ore, clay + v.clay, obsidian + v.obsidian, geode + v.geode)
    operator fun minus(v: Amount): Amount =
        Amount(ore - v.ore, clay - v.clay, obsidian - v.obsidian, geode - v.geode)
    operator fun times(n: Int): Amount =
        Amount(ore * n, clay * n, obsidian * n, geode * n)
}
typealias Cost = Amount
typealias Robots = Amount

object Day19Kt {
    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day19.in".readLines()
//            .split(String::isEmpty)
            .map { it.split(": ") }
            .map {
            val (id) = it.get(0).parse("Blueprint (\\d+).*")
            val robots = it.drop(1)
                .get(0).split(". ")
                .parse("\\s*Each (.*) robot costs (.*?)\\.?").associate { (minedOre, costs) ->
                    val costs = costs.split(" and ").parse("(\\d+) (.*)").associate { (cost, ore) ->
                        ore to cost.toInt()
                    }
                    val cost = Cost(costs["ore"] ?: 0, costs["clay"] ?: 0, costs["obsidian"] ?: 0, costs["geode"] ?: 0)
                    minedOre to cost
                }
            Blueprint(id.toInt(), robots)
        }

        fun howLongToCreateRobots(blueprint: Blueprint, current: Amount, robotsWorking: Robots, needed: Robots): Int  {
            val cost = blueprint.costOf(needed)
            val res = sequenceOf<(Amount) -> Int>({it.ore}, {it.clay}, {it.obsidian}, {it.geode})
                .filter { it(cost) > 0 }
                .map {
                if (it(robotsWorking) == 0) 10000 else ((it(cost) - it(current) + it(robotsWorking) - 1) / it(robotsWorking))
            }.max()
            return max(0, res)
        }

        data class State(val blueprint: Blueprint, val current: Amount, val robots: Robots, val timeLeft: Int)
        fun solve(cache: Cache<State, Int>, state: State, currentMax: AtomicInteger = AtomicInteger()): Int {
            return cache.get(state) {
                val maxGeodeIfNoNewRobots = state.current.geode + state.robots.geode * state.timeLeft
                if (maxGeodeIfNoNewRobots <= (currentMax.get() - (state.timeLeft * (state.timeLeft - 1) / 2))
                    || (state.robots.ore > 20 || state.robots.clay > 20) ) {
                    0
                } else {
                    val candidate = sequenceOf(
                        Robots(geode = 1),
                        Robots(obsidian = 1),
                        Robots(clay = 1),
                        Robots(ore = 1)
                    ).map {
                        val wait = howLongToCreateRobots(state.blueprint, state.current, state.robots, it) + 1
                        if (wait > state.timeLeft) {
                            0
                        } else {
                            val cost = state.blueprint.costOf(it)
                            val newCurrent = state.current + state.robots * wait - cost
                            solve(cache, State(state.blueprint, newCurrent, state.robots + it, state.timeLeft - wait), currentMax)
                        }
                    }.max()
                    val result = max(maxGeodeIfNoNewRobots, candidate)
                    currentMax.set(max(currentMax.get(), result))
                    result
                }
            }
        }

        val part1 = input.map {
            val cache = CacheBuilder.newBuilder().recordStats().build<State, Int>()
            val solved = solve(cache, State(it, Amount(), Robots(ore = 1), 24))
            println("${it.id} - $solved")
//            println(cache.stats())
//            println(maxGeodes)
            it.id * solved
        }.sum()
        println("part1: $part1")


        val part2 = input.take(3).map {
            val cache = CacheBuilder.newBuilder().recordStats().build<State, Int>()
            val solved = solve(cache, State(it, Amount(), Robots(ore = 1), 32))
            println("${it.id} - $solved")
            println(cache.stats())
            solved
        }.reduce{ a, b -> a * b }
        println("part2: $part2")


//        hu.plajko.utils.AoCKt.submitSolution(19, 1, part1)
    }
}
