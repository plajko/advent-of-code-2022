package hu.plajko.adventofcode2022

import hu.plajko.utils.CharMatrix
import hu.plajko.utils.readLines
import java.util.*
import kotlin.streams.asSequence

object Day23Kt {

    data class Pos(val x: Int, val y: Int) {
        fun n() = sequenceOf(Pos(x - 1, y - 1), Pos(x - 1, y), Pos(x - 1, y + 1)).toSet()
        fun s() = sequenceOf(Pos(x + 1, y - 1), Pos(x + 1, y), Pos(x + 1, y + 1)).toSet()
        fun w() = sequenceOf(Pos(x - 1, y - 1), Pos(x, y - 1), Pos(x + 1, y - 1)).toSet()
        fun e() = sequenceOf(Pos(x - 1, y + 1), Pos(x, y + 1), Pos(x + 1, y + 1)).toSet()
        fun neighbours() = sequenceOf(n(), s(), w(), e()).flatten().toSet()
    }

    fun Collection<Pos>.width() = this.maxOf { it.x } - this.minOf { it.x } + 1
    fun Collection<Pos>.height() = this.maxOf { it.y } - this.minOf { it.y } + 1

    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day23.in".readLines()
        val elfs = CharMatrix.fromList(input).stream().asSequence().filter { it.value == '#'.code }.map { Pos(it.row, it.column) }.toSet()
        fun simulate(limit: Int = 10000): Pair<Int, Set<Pos>> {
            var current = elfs
            val dirs = mutableListOf("N", "S", "W", "E")
            var i = 0
            while (true) {
                val next = current.asSequence().map { pos ->
                    when {
                        pos.neighbours().none { current.contains(it) } -> pos to pos
                        else -> {
                            dirs.asSequence().mapNotNull { dir ->
                                when {
                                    dir == "N" && pos.n().none { current.contains(it) } -> Pos(pos.x - 1, pos.y) to pos
                                    dir == "S" && pos.s().none { current.contains(it) } -> Pos(pos.x + 1, pos.y) to pos
                                    dir == "W" && pos.w().none { current.contains(it) } -> Pos(pos.x, pos.y - 1) to pos
                                    dir == "E" && pos.e().none { current.contains(it) } -> Pos(pos.x, pos.y + 1) to pos
                                    else -> null
                                }
                            }.firstOrNull() ?: (pos to pos)
                        }
                    }
                }.groupBy { it.first }.flatMap { (_, sameCoordinate) ->
                    if (sameCoordinate.size == 1) {
                        sameCoordinate.map { it.first }
                    } else {
                        sameCoordinate.map { it.second }
                    }
                }.toSet()
                if (i++ >= limit || current == next) {
                    break
                }
                current = next
                Collections.rotate(dirs, -1)
            }
            return i to current
        }
        val (_, part1Result) = simulate(10)
        val part1 = part1Result.width() * part1Result.height() - part1Result.size
        println("part1: $part1")

        val (part2, _) = simulate()
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(23, 1, part1)
    }
}
