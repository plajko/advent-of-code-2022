package hu.plajko.adventofcode2022

import com.google.common.util.concurrent.AtomicLongMap
import hu.plajko.utils.parse
import hu.plajko.utils.readLines
import hu.plajko.utils.split

object Day11Kt {

    data class Monkey(
        val id: Int,
        val items: MutableList<Long>,
        val operation: (Long) -> Long,
        val div: Long,
        val throwTo: (Long) -> Int
    )

    @JvmStatic
    fun main(args: Array<String>) {
        val monkeys = "Day11.in".readLines().split(String::isEmpty).map { m ->
            val ( id ) = m[0].parse(".*Monkey (\\d+):")
            val ( start ) = m[1].parse(".*Starting items: (.*)")
            val ( op1, op, op2 ) = m[2].parse(".*Operation: new = (.*) (.) (.*)")
            val ( div ) = m[3].parse(".*Test: divisible by (\\d+)")
            val ( ifTrue ) = m[4].parse(".*If true: throw to monkey (\\d+)")
            val ( ifFalse ) = m[5].parse(".*If false: throw to monkey (\\d+)")
            val divN = div.toLong()
            Monkey(
                id.toInt(),
                start.split(", ").map { it.toLong() }.toMutableList(),
                { n: Long ->
                    val op1v = if ("old" == op1) n else op1.toLong()
                    val op2v = if ("old" == op2) n else op2.toLong()
                    when (op) {
                        "*" -> op1v * op2v
                        "+" -> op1v + op2v
                        else -> throw IllegalStateException()
                    }
                },
                divN,
                { n: Long -> if (n % divN == 0L ) ifTrue.toInt() else ifFalse.toInt() }
            )
        }.associateBy { it.id }

        val part1 = solve(monkeys, 20) { it / 3 }
        val mod = monkeys.values.map { it.div }.reduce { a, b -> a * b }
        val part2 = solve(monkeys, 10000) { it % mod }
        println("part1: $part1")
        println("part2: $part2")
    }

    private fun solve(monkeys: Map<Int, Monkey>, numRounds: Int, coolDown: (Long) -> Long): Long {
        // create copy of state
        val state = monkeys.values.map {
                it.copy(items = mutableListOf<Long>().apply { addAll(it.items) })
            }.associateBy { it.id }
        val count = AtomicLongMap.create<Int>()
        repeat(numRounds) {
            (0 until state.size).forEach {
                val m = state[it]!!
                (0 until m.items.size).forEach { i ->
                    count.addAndGet(it, 1)
                    val newValue = coolDown(m.operation(m.items[i]))
                    val throwTo = m.throwTo(newValue)
                    val m2 = state[throwTo]!!
                    m2.items.add(newValue)
                }
                m.items.clear()
            }
        }
        return count.asMap().values.sorted().takeLast(2).reduce { a,b -> a * b }
    }
}
