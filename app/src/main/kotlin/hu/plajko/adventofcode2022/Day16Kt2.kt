package hu.plajko.adventofcode2022

import com.google.common.cache.CacheBuilder
import hu.plajko.utils.GraphWrapperKt
import hu.plajko.utils.parse
import hu.plajko.utils.readLines
import org.jgrapht.alg.shortestpath.DijkstraShortestPath
import kotlin.math.max

object Day16Kt2 {

    data class Valve(val id: String, val rate: Long)

    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day16.in".readLines().map { it.split("; ") }.map { (p1, p2) ->
            val (v, r) = p1.parse("Valve (.*) has flow rate=(.*)")
            val (tunnels) = p2.parse("tunnels? leads? to valves? (.*)")
            Pair(Valve(v, r.toLong()), tunnels.split(", "))
        }
        val valves = input.map { it.first }.associateBy { it.id }
        val g = GraphWrapperKt.newDirected<Valve>()
        input.forEach { (valve, tunnels) ->
            tunnels.forEach {
                g.addEdge(valve, valves[it]!!) { 1.0 }
            }
        }
        val d = DijkstraShortestPath(g.graph)
        val paths = valves.keys.associateWith { d.getPaths(valves[it]!!) }
        val distanceCache = CacheBuilder.newBuilder().recordStats().build<Pair<Valve, Valve>, Int>()
        fun getDistance(valves: Pair<Valve, Valve>): Int {
            return distanceCache.get(valves) {
                paths[valves.first.id]!!.getPath(valves.second).length
            }
        }
        val startValve = valves["AA"]!!
        val toOpen = valves.values.filter { it.rate > 0 }

        data class State(val current: Valve, val opened: Set<Valve>, val time: Int, val timeLimit: Int, val elephant: Boolean = false)
        val cache = CacheBuilder.newBuilder().recordStats().build<State, Long>()
        fun solve(state: State): Long {
            return cache.get(state) {
                val elephantResult = if (state.elephant) solve(State(startValve, state.opened, 0, state.timeLimit, false)) else 0
                val results = toOpen.filter { !state.opened.contains(it) }
                    .map { it to state.time + getDistance(state.current to it) + 1 }
                    .filter { (_, newTime) -> newTime < state.timeLimit}
                    .map { (candidate, newTime) ->
                        val result = solve(State(candidate, state.opened + candidate, newTime, state.timeLimit, state.elephant), )
                        result + (state.timeLimit - newTime) * candidate.rate
                    }
                max(if(results.isEmpty()) 0 else results.max(), elephantResult)
            }
        }

        val part1 = solve(State(startValve, setOf(), 0, 30))
        println("part1: $part1")

        val part2 = solve(State(startValve, setOf(), 0, 26, true))
        println("part2: $part2")
        println(distanceCache.stats())
        println(cache.stats())
    }
}
