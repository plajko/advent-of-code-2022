package hu.plajko.utils;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.ExtensionMethod;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@ExtensionMethod(Extensions.class)
public class CharMatrix {

    @Value
    public class Cell {
        public int row;
        public int column;
        public int value;

        public void set(char c) {
            setIf(c, cell -> true);
        }

        public void setIf(char c, Predicate<Cell> predicate) {
            if (predicate.test(this)) {
                matrix[row][column] = c;
            }
        }
    }

    @Getter
    private final int[][] matrix;

    public CharMatrix(int numRows, int numCols) {
        this.matrix = new int[numRows][numCols];
        Arrays.stream(this.matrix).forEach(it -> Arrays.fill(it, '.'));
    }

    public CharMatrix(int[][] matrix) {
        this.matrix = matrix;
    }

    private CharMatrix(List<String> lines) {
        this(lines.stream().map(line -> line.chars().toArray()).toArray(int[][]::new));
    }

    public static CharMatrix fromList(List<String> lines) {
        int numCols = lines.stream().mapToInt(String::length).max().orElseThrow();
        return new CharMatrix(lines.stream().map(line -> Arrays.copyOf(line.chars().toArray(), numCols)).toArray(int[][]::new));
    }

    public int getNumRows() {
        return matrix.length;
    }

    public int getNumCols() {
        return matrix.length == 0 ? 0 : matrix[0].length;
    }

    public String getRow(int rowIndex) {
        return Arrays.stream(matrix[rowIndex]).mapToObj(Character::toString).collect(Collectors.joining());
    }

    public String getColumn(int columnIndex) {
        return Arrays.stream(matrix).map(row -> row[columnIndex]).map(Character::toString).collect(Collectors.joining());
    }

    public char get(int rowIndex, int columnIndex) {
        return (char) matrix[rowIndex][columnIndex];
    }

    public void set(int rowIndex, int columnIndex, char c) {
        matrix[rowIndex][columnIndex] = c;
    }

    public void set(int rowIndex, int columnIndex, int c) {
        matrix[rowIndex][columnIndex] = c;
    }

    public Stream<Cell> stream() {
        return IntStream.range(0, getNumRows()).boxed()
                .flatMap(rowIndex -> IntStream.range(0, getNumCols())
                        .mapToObj(columnIndex -> new Cell(rowIndex, columnIndex, (char) matrix[rowIndex][columnIndex]))
                );
    }

    public void setColumn(int columnIndex, String column) {
        IntStream.range(0, matrix.length).forEach(rowIndex -> matrix[rowIndex][columnIndex] = column.charAt(rowIndex));
    }

    public void setRow(int rowIndex, String row) {
        IntStream.range(0, row.length()).forEach(columnIndex -> matrix[rowIndex][columnIndex] = row.charAt(columnIndex));
    }

    @Override
    public String toString() {
        return Arrays.stream(matrix)
                .map(r -> Arrays.stream(r).mapToObj(Character::toString).collect(Collectors.joining()))
                .collect(Collectors.joining("\n")) + "\n";
    }

    public CharMatrix copy() {
        return new CharMatrix(Arrays.stream(matrix).map(int[]::clone).toArray(int[][]::new));
    }

    public List<List<Character>> toList() {
        return Arrays.stream(matrix)
                .map(r -> Arrays.stream(r).mapToObj(c -> (char) c).collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    public static CharMatrix fromCharList(List<List<Character>> list) {
        return fromList(list, Function.identity());
    }

    public static CharMatrix fromIntList(List<List<Integer>> list) {
        return fromList(list, i -> i != null ? (char) i.intValue() : ' ');
    }

    public static CharMatrix fromStringList(List<List<String>> list) {
        return fromList(list, s -> s != null && s.length() > 0 ? s.charAt(0) : ' ');
    }

    private static <T> CharMatrix fromList(List<List<T>> list, Function<T, Character> converter) {
        return new CharMatrix(list.stream()
                .map(l -> l.stream()
                        .map(converter)
                        .map(c -> Character.toString(c))
                        .collect(Collectors.joining()))
                .collect(Collectors.toList()));
    }

    public static Color fromHexColor(String hexValue) {
        return new Color(0xff000000 | Integer.parseInt(hexValue.startsWith("#") ? hexValue.substring(1) : hexValue, 16));
    }

    @SneakyThrows
    public void renderImage(File f, Function<Integer, String> colorProvider) {
        int scale = 3;
        var img = new BufferedImage(getNumCols() * scale, getNumRows() * scale, BufferedImage.TYPE_INT_ARGB);
        var g2d = img.createGraphics();
        for (int x = 0; x < getNumCols(); x++) {
            for (int y = 0; y < getNumRows(); y++) {
                int c = matrix[y][x];
                var colorString = colorProvider.apply(c);
                if (colorString != null) {
                    Color color = fromHexColor(colorString);
                    g2d.setColor(color);
                    g2d.fillRect(x * scale, y * scale, scale, scale);
                }
            }
        }
        g2d.dispose();
        ImageIO.write(img, "png", f);
    }

    public CharMatrix subMatrix(int r1, int c1, int r2, int c2) {
        var a = Lists.<String>newArrayList();
        for (int i = r1; i < r2; i++) {
            a.add(getRow(i).substring(c1, c2));
        }
        return CharMatrix.fromList(a);
    }

    public void set(int r1, int c1, CharMatrix m) {
        for (int i = r1; i < r1 + m.getNumRows(); i++) {
            var r = getRow(i);
            var nr = r.substring(0, c1) + m.getRow(i - r1) + r.substring(c1 + m.getNumCols());
            setRow(i, nr);
        }
    }

    public CharMatrix rotateCW() {
        return CharMatrix.fromCharList(toList().rotateCW());
    }

    public CharMatrix rotateCCW() {
        return CharMatrix.fromCharList(toList().rotateCCW());
    }
}

