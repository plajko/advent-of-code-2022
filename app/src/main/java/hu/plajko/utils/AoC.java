package hu.plajko.utils;

import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;

import java.io.File;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@ExtensionMethod({Extensions.class})
public class AoC {

    private static final int YEAR = 2022;

    private static String getSessionId() {
        return "session_id".readLines().first();
    }

    @SneakyThrows
    public static Path downloadInput(int day) {
        var uri = new URI(String.format("https://adventofcode.com/%s/day/%s/input", YEAR, day));
        LOG.info("Trying to fetch input from {}", uri);
        var client = HttpClient.newBuilder().build();
        var request = HttpRequest.newBuilder()
            .header("Cookie", String.format("session=%s", getSessionId()))
            .uri(uri).build();
        var resp = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
        if (resp.statusCode() == 200) {
            var fileName = "Day" + Integer.toString(day).pad(2, '0') + ".in";
            var path = new File(new File("app\\src\\main\\resources"), fileName).toPath();
            Files.write(path, resp.body());
            return path;
        } else {
            throw new IllegalArgumentException(new String(resp.body(), StandardCharsets.UTF_8));
        }
    }

    private static String getFormDataAsString(Map<String, String> formData) {
        return formData.entrySet().stream()
            .map(e -> String.format("%s=%s",
                URLEncoder.encode(e.getKey(), StandardCharsets.UTF_8),
                URLEncoder.encode(e.getValue(), StandardCharsets.UTF_8)))
            .collect(Collectors.joining("&"));
    }

    @SneakyThrows
    public static void submitSolution(int day, int level, Object answer) {
        var uri = new URI(String.format("https://adventofcode.com/%s/day/%s/answer", YEAR, day));
        var client = HttpClient.newBuilder().build();
        var body = getFormDataAsString(Map.of(
            "level", Integer.toString(level),
            "answer", String.valueOf(answer)));
        var request = HttpRequest.newBuilder()
            .POST(HttpRequest.BodyPublishers.ofString(body, StandardCharsets.UTF_8))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .header("Cookie", String.format("session=%s", getSessionId()))
            .uri(uri).build();
        var resp = client.send(request, HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));
        var page = Jsoup.parse(resp.body());
        LOG.info("{}", page.select("body > main > article").text());
    }
}
