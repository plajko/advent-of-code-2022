package hu.plajko.adventofcode2022

import hu.plajko.utils.readLines
import kotlin.math.absoluteValue

object Day20Kt {

    data class Item(val index: Int, var value: Int) {
        var next: Item? = null
        var prev: Item? = null
        fun shift(t: (Int) -> Int = { it }) {
            val shift = t(value)
            if (shift > 0) {
                repeat(shift) { swap(next!!) }
            } else if (shift < 0) {
                repeat(-shift) { prev!!.swap(this) }
            }
        }
        operator fun plus(i: Int): Item {
            return (1..i.absoluteValue)
                .fold(this) { c, _ -> if (i > 0) c.next!! else c.prev!! }
        }
        private fun swap(target: Item) {
            target.prev = this.prev.also {  this.next = target.next }
            target.next!!.prev = this.also { this.prev!!.next = target  }
            this.prev = target.also { target.next = this }
        }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        val input = {
            "Day20.in".readLines().mapIndexed { i, n -> Item(i, n.toInt()) }.also {
                it.first().prev = it.last()
                it.last().next = it.first()
                it.windowed(2) { (a, b) ->  a.next = b.also { b.prev = a } }
            }
        }

        val sum: (Item) -> Int = { (1..3).sumOf { i -> (it + i * 1000).value } }

        val part1Items = input()
        part1Items.forEach { it.shift() }
        val part1 = part1Items.find { it.value == 0 }!!.let(sum)
        println("part1: $part1")

        val decryptionKey = 811589153L
        val part2Items = input()
        repeat(10) {
            part2Items.forEach { item -> item.shift { ((decryptionKey * it) % (part2Items.size - 1)).toInt() } }
        }
        val part2 = decryptionKey * part2Items.find { it.value == 0 }!!.let(sum)
        println("part2: $part2")
//        hu.plajko.utils.AoCKt.submitSolution(20, 2, part1)
    }
}
