package hu.plajko.adventofcode2022

import com.google.common.cache.CacheBuilder
import hu.plajko.utils.GraphWrapperKt
import hu.plajko.utils.parse
import hu.plajko.utils.readLines
import org.jgrapht.alg.shortestpath.DijkstraShortestPath
import kotlin.math.min


object Day16Kt1 {

    data class State(var released: Long = 0, var time: Int = 30, var rate: Long = 0) {
        fun open(v: String, r: Long): Boolean {
            if (time <= 0) {
                throw IllegalStateException()
            }
            time--
            released += rate
            rate += r
            return time > 0
        }

        fun step(l: Int): Boolean {
            if (time <= 0) {
                throw IllegalStateException()
            }
            repeat(l) {
                time--
                released += rate
                if (time == 0) {
                    return false
                }
            }
            return true
        }

        fun calcReleased(): Long {
            val res = released + time * rate
            return res
        }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        val input = "Day16s.in".readLines().map { it.split("; ") }.map { (p1, p2) ->
            val (v, r) = p1.parse("Valve (.*) has flow rate=(.*)")
            val (tunnels) = p2.parse("tunnels? leads? to valves? (.*)")
            Triple(v, r.toLong(), tunnels.split(", "))
        }
        val index = input.associateBy { it.first }
        val nodes = input.filter { it.second > 0 }.map { it.first }.toSet()
        val g = GraphWrapperKt.newDirected<String>()
        input.forEach { v ->
            v.third.forEach {
                g.addEdge(v.first, it) { 1.0 }
            }
        }
        val d = DijkstraShortestPath(g.graph)
        val paths = (nodes + "AA").associateWith { d.getPaths(it) }

        data class State2(val v1: String?, val v2: String, val notOpenedYet: Set<String>, val timeLeft: Int)
        val cache = CacheBuilder.newBuilder().build<State2, Pair<Long, List<String>>>()
        fun costOfOpening(v1: String?, v2: String, notOpenedYet: Set<String>, path: List<String>, timeLeft: Int): Pair<Long, List<String>> {
            return cache.get(State2(v1, v2, notOpenedYet, timeLeft)) {
                val time2 = min(timeLeft, if (v1 == null) 0 else (paths[v1]!!.getPath(v2).length + 1))
                val loss = notOpenedYet.map { index[it]!! }.sumOf { it.second }
                var result = time2 * loss
                val notOpened = notOpenedYet - v2
                if(notOpened.isEmpty() || timeLeft - time2 < 0) {
                    Pair(result, path)
                } else {
                    val min = notOpened.map { it to costOfOpening(v2, it, notOpened,  path, timeLeft - time2) }.minBy { it.second.first }
                    result += min.second.first
                    Pair(result, listOf(min.first)  + min.second.second)
                }
            }
        }

        val sum = nodes.sumOf { index[it]!!.second } * 30
        val cost = costOfOpening(null, "AA", nodes.toSet(), listOf(), 30)
        println(sum - cost.first)
        println(cost.second)

        fun calcPressure(order: List<String>): State {
            val s = State()
            var cont = s.step(paths["AA"]!!.getPath(order.first()).length)
            if (!cont) {
                return s
            }
            order.windowed(2).forEach { (v1, v2) ->
                cont = s.open(v1, index[v1]!!.second)
                if (!cont) {
                    return s
                }
                val step = paths[v1]!!.getPath(v2).length
                cont = s.step(step)
                if (!cont) {
                    return s
                }
            }
            cont = s.open(order.last(), index[order.last()]!!.second)
            if (!cont) {
                return s
            }
            s.step(30)
            return s
        }
        val part1 = calcPressure(cost.second).calcReleased()
        println("part1: $part1")
//        val max = (0 until cost.second.size - 1).map {
//            it to calcPressure(cost.second.dropLast(it)).calcReleased()
//        }.forEach{
//            println(it)
//        }
//        println(paths["MJ"]!!.getPath("YJ").length)

//        println(max)
//        println("part1: $part1")

//        hu.plajko.utils.AoCKt.submitSolution(16, 1, part1)
    }
}
